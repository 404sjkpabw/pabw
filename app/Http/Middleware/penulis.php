<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class penulis
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
          if (Auth::user() &&  Auth::user()->role_id == 0) {
                 return $next($request);
          }
          if (Auth::user() &&  Auth::user()->role_id == 1){
            return redirect()->back();
          }else{

         return redirect('/masuk');}
     }
}
