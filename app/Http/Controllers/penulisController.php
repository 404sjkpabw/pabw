<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\penulis;
use Auth;
class penulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {$nama=Auth::user()->nama;
        $penulis=penulis::all();
        return view('admin/penulis/index',compact('penulis','nama'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/penulis/tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $penulis = new penulis();
      $penulis->nama = $request->nama;
      $penulis->email = $request->email;
      $penulis->nik = $request->nik;
      $penulis->nohp = $request->hp;
      $penulis->save();
        return redirect('penulis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {$nama=Auth::user()->nama;
        $penulis=penulis::find($id);
        return view('admin/penulis/edit',compact('penulis','nama'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $penulis = penulis::find($id);
      $penulis->nama = $request->nama;
      $penulis->email = $request->email;
      $penulis->nik = $request->nik;
      $penulis->nohp = $request->hp;
      $penulis->save();
        return redirect('penulis')->with('success','Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $delete = penulis::find($id);
    $delete -> delete();
    return redirect('penulis');
    }
}
