<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use Auth;
class kreatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {$nama=Auth::user()->nama;
      $berita=berita::where('id_penulis',Auth::user()->id)->get();
      $tolak =0;
      $acc=0;
      $waiting=0;
      foreach ($berita as $hitung) {
        if($hitung->status==2){
          $tolak+=1;
        }
        elseif ($hitung->status==1) {
          $acc+=1;
        }
        else{
          $waiting+=1;
        }
      }
      return view('kontributor.index',compact('berita','tolak','acc','waiting','nama'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $nama=Auth::user()->nama;
            return view('kontributor.tambah',compact('nama'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [

        'filename' => 'required',
        'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

    ]);
      function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
      if($request->hasfile('filename'))
       {

        $image=  $request->file('filename');

              $name=generateRandomString().'.'.$image->getClientOriginalExtension();
              $image->move(public_path().'/images/upload/', $name);
              $data = $name;

       }

    $berita = new berita();
      $berita->judul =  $request->judul;
      $berita->isi = $request->isi;
      $berita->id_penulis = Auth::user()->id;
      $berita->gambar = $data;
      $berita->status = 3;
      $berita -> save();

      return redirect('penulis/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $berita=berita::find($id);
      $nama=Auth::user()->nama;
      return view('kontributor.edit',compact('berita','nama'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [


        'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

    ]);
      function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
      if($request->hasfile('filename'))
       {

        $image=  $request->file('filename');

              $name=generateRandomString().'.'.$image->getClientOriginalExtension();
              $image->move(public_path().'/images/upload/', $name);
              $data = $name;

       }

    $berita = berita::find($id);
      $berita->judul =  $request->judul;
      $berita->isi = $request->isi;
      $berita->id_penulis = Auth::user()->id;
      if($request->hasfile('filename'))
       {$berita->gambar = $data;}

      $berita -> save();

      return redirect('penulis/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $delete = berita::find($id);
    $delete -> delete();
    return redirect('/penulis/dashboard');
    }
}
