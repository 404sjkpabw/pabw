<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\berita;
use App\penulis;
use Auth;
class beritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function status(Request $request, $id)
     {


     $berita = berita::find($id);
       $berita->status =  $request->input('action');

$berita->save();

       return redirect('/validasi');
     }

    public function index()
    {$nama=Auth::user()->nama;
      $berita=berita::all();
      $tolak=berita::where('status',2)->get();
      $acc=berita::where('status',1)->get();
      $waiting=berita::where('status',3)->get();
      return view('admin.berita.index',compact('berita','tolak','acc','waiting','nama'));
    }
    public function validasiberita()
    {
$nama=Auth::user()->nama;
      $berita=berita::where('status',3)->get();
      return view('admin.berita.validasi',compact('berita','nama'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {$nama=Auth::user()->nama;
        $penulis=penulis::all();
        return view('admin.berita.tambah',compact('penulis','nama'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      $this->validate($request, [

        'filename' => 'required',
        'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

    ]);
      function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
      if($request->hasfile('filename'))
       {

        $image=  $request->file('filename');

              $name=generateRandomString().'.'.$image->getClientOriginalExtension();
              $image->move(public_path().'/images/upload/', $name);
              $data = $name;

       }

    $berita = new berita();
      $berita->judul =  $request->judul;
      $berita->isi = $request->isi;
      $berita->id_penulis = $request->penulis;
      $berita->gambar = $data;
      $berita->status = 3;
      $berita -> save();

      return redirect('berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      $nama=Auth::user()->nama;
      $berita=berita::all();
      $tolak=berita::where('status',2)->get();
      $acc=berita::where('status',1)->get();
      $waiting=berita::where('status',3)->get();
        $penulis=penulis::all();
      return view('admin.index',compact('berita','tolak','acc','waiting','penulis','nama'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {$nama=Auth::user()->nama;
      $berita=berita::find($id);
      $penulis=penulis::all();
      return view('admin.berita.edit',compact('berita','penulis','nama'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [


        'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

    ]);
      function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
      if($request->hasfile('filename'))
       {

        $image=  $request->file('filename');

              $name=generateRandomString().'.'.$image->getClientOriginalExtension();
              $image->move(public_path().'/images/upload/', $name);
              $data = $name;

       }

    $berita = berita::find($id);
      $berita->judul =  $request->judul;
      $berita->isi = $request->isi;
      $berita->id_penulis = $request->penulis;
      if($request->hasfile('filename'))
       {$berita->gambar = $data;}

      $berita -> save();

      return redirect('berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $delete = berita::find($id);
    $delete -> delete();
    return redirect('berita');
    }
}
