<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use Auth;
use Session;

class AuthController extends Controller
{
      public function validation($request){
      return $this->validate($request, [
        'nama' => 'required|max:255',
        'nik' => 'required|max:255',
        'nohp' => 'required|max:255',
        'email' => 'required|max:255|unique:users',
        'password' => 'required|min:6|max:255',
      ]);
    }
    public function daftar(){
      return view('auth.register');
    }
    public function daftarkan(Request $request){
      $this->validation($request);
      $request['password'] = bcrypt($request->password);

      $daftar = new user;
      $daftar -> nama = $request -> nama;
      $daftar -> password = $request -> password;
      $daftar -> nik = $request -> nik;
      $daftar -> email = $request -> email;
      $daftar -> nohp = $request -> nohp;
      $daftar -> role_id = 0;

      $daftar -> save();
      return redirect()->back()->with('success', ' Daftar berhasil');

    }
    public function masuk(){

      return view('auth.index');
    }
    public function masukkan(Request $request){
      $this -> validate($request,[
        'email' => 'required|max:255',
        'password' => 'required|max:255',
      ]);
      if(Auth::attempt(['email' => $request -> email,'password' => $request -> password, 'role_id' => 0])){
        return redirect('/penulis/dashboard');
      }
      if(Auth::attempt(['email' => $request -> email,'password' => $request -> password, 'role_id' => 1])){
        return redirect('/admin');
      }
      return redirect()->back()->with('failed','Login Gagal !');
    }
    public function getLogout(){
      Auth::logout();
      Session::flush();
      return Redirect('/masuk');
    }
}
