<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\berita;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $berita=berita::paginate(3);
      $jabar = file_get_contents('http://adiviagp.com/jabar.json');
      $jabar =json_decode($jabar);
      $jatim = file_get_contents('http://adiviagp.com/jatim.json');
      $jatim =json_decode($jatim);
      $jateng = file_get_contents('http://adiviagp.com/jateng.json');
      $jateng =json_decode($jateng);
      return view('landing',compact('berita','jatim','jateng','jabar'));

        }
      public function blog($id)
      {
       $berita=berita::find($id);
        return view('blog',compact('berita'));    }
}
