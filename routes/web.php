<?php

/*
|--------------------------------------------------------------------------
|                          ROUTES UNTUK PENULIS
|--------------------------------------------------------------------------
*/

Route::get('/', 'homeController@index');
Route::get('/blog/{id}', 'homeController@blog');

Route::get('/daftar','AuthController@daftar')->name('register');
Route::post('/daftar','AuthController@daftarkan')->name('daftarkan');

Route::get('/masuk','AuthController@masuk')->name('login');
Route::post('/masuk','AuthController@masukkan')->name('masukkan')->middleware('dahmasok');
Route::post('/logout','AuthController@getLogout')->name('logout');

Route::group(['middleware' => 'penulis'], function () {
  Route::get('/penulis/dashboard','kreatorController@index');
Route::get('/penulis/dashboard/berita/tambah','kreatorController@create');
  Route::post('penulis/dashboard/berita/tambah','kreatorController@store')->name('simpankreator');

  Route::get('/penulis/dashboard/berita/edit/{id}','kreatorController@edit')->name('tampiledit');
  Route::put('/penulis/dashboard/berita/edit/{id}','kreatorController@update')->name('simpanedit');
  Route::delete('/penulis/dashboard/berita/delete/{id}','kreatorController@destroy')->name('hapuskreator');
});
/*
|--------------------------------------------------------------------------
|                          ROUTES UNTUK BERITA
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'admin'], function () {
  Route::get('/penulis','penulisController@index');
  Route::post('/penulis/tambah','penulisController@store');
  Route::get('/penulis/tambah','penulisController@create');
  Route::get('/penulis/edit/{id}','penulisController@edit');
  Route::put('/penulis/edit/{id}','penulisController@update');
  Route::delete('/penulis/delete/{id}','penulisController@destroy');
Route::get('/admin','beritaController@show');
Route::get('/berita','beritaController@index');
Route::get('/validasi','beritaController@validasiberita');
Route::put('/validasi/{id}','beritaController@status');
Route::post('/berita/tambah','beritaController@store');
Route::get('/berita/tambah','beritaController@create');
Route::get('/berita/edit/{id}','beritaController@edit');
Route::put('/berita/edit/{id}','beritaController@update');
Route::delete('/berita/delete/{id}','beritaController@destroy');

});
