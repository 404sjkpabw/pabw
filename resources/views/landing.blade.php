<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="../assets/img/favicon.ico">
	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>PORTAL EDUKASI KESIAPSIAGAAN BENCANA</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/paper-kit.css?v=2.1.0')}}">


	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="{{asset('css/demo.css')}}" rel="stylesheet" />

	<!--     Fonts and icons     -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{asset('css/nucleo-icons.css')}}" rel="stylesheet">

</head>

<body>
	<nav class="navbar navbar-expand-lg fixed-top bg-danger" >
		<div class="container">
			<div class="navbar-translate">
				<a class="navbar-brand" href="/#depan">404 - TEAM</a>
				<button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="/#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a href="/#depan" target="_self"  class="nav-link">HOME</a>
					</li>
					<li class="nav-item">
						<a href="/#siaga" target="_self" class="nav-link">SIAGA</a>
					</li>
					<li class="nav-item">
						<a href="/#berita" target="_self" class="nav-link"></i>BERITA</a>
					</li>
					<li class="nav-item">
						<a href="/#team" target="_self" class="nav-link"></i>TEAM</a>
					</li>
					<li class="nav-item">
						<a href="/masuk" target="_self" class="nav-link"></i>LOGIN</a>
					</li>

				</ul>
			</div>
		</div>
	</nav>
<section id="depan">
	<div class="page-header" data-parallax="true" style="background-image: url('{{asset('images/daniel-olahh.jpg')}}');">
		<div class="filter"></div>
		<div class="container">
			<div class="motto text-center">
				<h1>PORTAL EDUKASI KESIAPSIAGAAN BENCANA</h1>

				<br />

			</div>
		</div>
	</div>
</section>
	<div class="main">
		<div class="section text-center" id="siaga">
			<div class="container-fluid">

				<br /><br />
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-6 pull-right">
							<h3 class="title text-left">SIAGA BENCANA
								<hr style="width:30%;border-top: 5px solid /#F7941D;" size="10" align="left">
							</h3><br><br>
							<h6 class="description text-left">Pengetahuan yang diperlukan dalam menghadapi bencana mencakup pencegahan terjadinya bencana, ciri-ciri akan terjadinya bencana, hal-hal yang harus atau jangan dilakukan saat terjadi bencana, dan
								langkah-langkah yang perlu diakukan setelah bencana terjadi.</h6>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-2">
								<div class="info">

									<img src="{{asset('images/015-earthquake-1.png')}}" alt="..." class="img-thumbnail"></i>

									<div class="description">
										<h4 class="info-title">Gempa</h4>

									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="info">
									<div class="icon icon-danger">
										<img src="{{asset('images/048-tsunami.png')}}" alt="..." class="img-thumbnail">
									</div>
									<div class="description">
										<h4 class="info-title">Tsunami</h4>


									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="info">
									<div class="icon icon-danger">
										<img src="{{asset('images/028-volcano-1.png')}}" alt="..." class="img-thumbnail">
									</div>
									<div class="description">
										<h4 class="info-title">Gunung Berapi</h4>


									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="info">
									<div class="icon icon-danger">
										<img src="{{asset('images/003-fire.png')}}" alt="..." class="img-thumbnail">
									</div>
									<div class="description">
										<h4 class="info-title">Kebakaran Hutan</h4>


									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="info">
									<div class="icon icon-danger">
										<img src="{{asset('images/049-twister.png')}}" alt="..." class="img-thumbnail">
									</div>
									<div class="description">
										<h4 class="info-title">Angin Tornado</h4>


									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="info">
									<div class="icon icon-danger">
										<img src="{{asset('images/004-landslide-2.png')}}" alt="..." class="img-thumbnail">
									</div>
									<div class="description">
										<h4 class="info-title">Longsor</h4>


									</div>
								</div>
							</div>
						</div>
					</div>


				</div>


			</div>
		</div>
		<div class="section section-gray" id="berita">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 class="font-weight-bold">BERITA TERKINI</h2>
						<hr style="width:30%;border-top: 5px solid /#F7941D;" size="10">
						<br>
					</div>
				</div>
				<div class="row">
					@foreach ($berita as $beritas)


					<div class="blog col-md-4 text-center">
						<div class="col-md-12" style="background-color:white;">
							<br>
							<img class="img-rounded img-responsive" src="{{asset('images/upload/'.$beritas->gambar)}}" style="height:300px;">
							<h3 class="title text-left">{{$beritas->judul}}</h3>
							<p class="description text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
								ex ea commodo consequat.</p>
							<br><button class="btn btn-danger btn-round"><a href="/#siaga">SEE MORE</a></button><br><br>
						</div>
					</div>
					@endforeach

				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<br>
						<button class="btn-link">{{ $berita->links() }}</button>
						<hr style="width:100%;border-top: 5px solid gray;" size="10">
						<br>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4" style="background-color:white;">
						<h2 class="title text-center font-weight-bold">BPBD JATIM</h2>
						<hr style="width:60%;border-top: 5px solid gray;" size="10">
						@foreach($jatim as $jatim)
						<div class="row">

							<div class="blog col-md-12 text-center">

								<div class="col-md-12">
									<br>
									<img class="img-rounded img-responsive" src="{{$jatim->gambar}}">
									<h3 class="title text-left">{{$jatim->judul}}</h3>
									<p class="description text-left">{{$jatim->isi}}</p>
									<br><button class="btn btn-danger btn-round" ><a href="{{$jatim->link}}">SEE MORE</a></button><br><br>
								</div>
							</div>
						</div>
					@endforeach
					</div>
					<div class="col-md-4" style="background-color:white;">
						<h2 class="title text-center font-weight-bold">BPBD JATENG</h2>
						<hr style="width:60%;border-top: 5px solid gray;" size="10">
						@foreach($jateng as $jateng)
						<div class="row">

							<div class="blog col-md-12 text-center">

								<div class="col-md-12">
									<br>

									<h3 class="title text-left">{{$jateng->judul}}</h3>
									<p class="description text-left">{{$jateng->isi}}</p>
									<br><button class="btn btn-danger btn-round"><a href="{{$jateng->link}}">SEE MORE</a></button><br><br>
								</div>
							</div>
						</div>
						@endforeach

					</div>
					<div class="col-md-4" style="background-color:white;">
						<h2 class="title text-center font-weight-bold">BPBD JABAR</h2>
						<hr style="width:60%;border-top: 5px solid gray;" size="10">
						@foreach($jabar as $jabar)
						<div class="row">

							<div class="blog col-md-12 text-center">

								<div class="col-md-12">
									<br>
									<img class="img-rounded img-responsive" src="http://bpbd.jabarprov.go.id/.{{$jabar->gambar}}">
									<h3 class="title text-left">{{$jabar->judul}}</h3>
									<p class="description text-left">{{$jabar->isi}}</p>
									<br><button class="btn btn-danger btn-round"><a href="http://bpbd.jabarprov.go.id/.{{$jabar->link}}">SEE MORE</a></button><br><br>
								</div>
							</div>
						</div>
						@endforeach

					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-center">
						<br>

						<hr style="width:100%;border-top: 5px solid gray;" size="10">
						<br>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-light text-center" id="team">
			<div class="container text-center">
				<h1>Ayo Bergabung Bersama kami Menjadi Kontributor</h1><br><br>
				<a href="/daftar" class="btn btn-warning">JOIN US</a>
			</div>
		</div>
		<div class="section section-dark text-center" id="team">
			<div class="container">
				<h2 class="title">Let's talk about us</h2>
				<div class="row">
					<div class="col-md-4">
						<div class="card card-profile card-plain">

							<div class="card-body">
								<a href="/#paper-kit">
									<div class="author">
										<h4 class="card-title">Achmad Khodzim</h4>
										<h6 class="card-category">Full Stack Dev</h6>
									</div>
								</a>

							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="card card-profile card-plain">

							<div class="card-body">
								<a href="/#paper-kit">
									<div class="author">
										<h4 class="card-title">Arga Arif</h4>
										<h6 class="card-category">Designer</h6>
									</div>
								</a>

							</div>

						</div>
					</div>

					<div class="col-md-4">
						<div class="card card-profile card-plain">

							<div class="card-body">
								<a href="/#paper-kit">
									<div class="author">
										<h4 class="card-title">Fikri Auliyak</h4>
										<h6 class="card-category">Front End</h6>
									</div>
								</a>

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="footer section-dark">
		<div class="container">
			<div class="row">

				<div class="credits ml-auto">
					<span class="copyright">
						© <script>
							document.write(new Date().getFullYear())
						</script>, made with <i class="fa fa-heart heart"></i> by 404 Team
					</span>
				</div>
			</div>
		</div>
	</footer>
</body>

<!-- Core JS Files -->

<script src="{{asset('js/jquery-3.2.1.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/popper.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Paper Kit Initialization snd functons -->
<script src="{{asset('js/paper-kit.js?v=2.1.0')}}"></script>

</html>
