<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Login kontributor</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{asset('css/paper-kit.css?v=2.1.0')}}" rel="stylesheet"/>

	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="{{asset('css/demo.css')}}" rel="stylesheet" />

    <!--     Fonts and icons     -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/nucleo-icons.css')}}" rel="stylesheet">

</head>
<body>
    <nav class="navbar navbar-expand-md fixed-top navbar-transparent">
        <div class="container">
			<div class="navbar-translate">
	            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
	            </button>
	            <a class="navbar-brand" href="/#team">404 - Team</a>
			</div>

		</div>
    </nav>
    <div class="wrapper">
        <div class="page-header" style="background-image: url({{asset('images/login-image.jpg')}})">
            <div class="filter"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 ml-auto mr-auto">
                            <div class="card card-register">
                                <h3 class="title">Welcome</h3>
								<div class="social-line text-center">
									@if (count($errors) > 0)
										<div class="alert alert-danger imgdiv">
												<strong>Whoops!</strong> There were some problems with your input.
												<ul>
									<!-- print  error -->
														@foreach ($errors->all() as $error_val)
																<li>{{ $error_val }}</li>
														@endforeach
												</ul>
										</div>
								@endif
								@if ($success_message = Session::get('failed'))
								<div class="alert alert-danger alert-block imgdiv">
										<button type="button" class="close imgdiv" data-dismiss="alert">×</button>
												<strong>{{ $success_message }}</strong>
								</div>
								@endif
                                </div>
																<form method="POST" action="{{ route('masukkan') }}">
						                        @csrf
                                    <label>Email</label>
																		<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
																		@if ($errors->has('email'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
																		<label>Password</label>
																		<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

																		@if ($errors->has('password'))
																				<span class="invalid-feedback" role="alert">
																						<strong>{{ $errors->first('password') }}</strong>
																				</span>
																		@endif
																		<button class="btn btn-danger btn-block btn-round" type="submit">Login</button>
                                </form>
                                <div class="forgot">
																	<a class="btn btn-danger btn-block btn-round" href="/daftar">Daftar</a>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="footer register-footer text-center">
						<h6>&copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart">LOVE</i> by 404 Team</h6>
					</div>
                </div>
        </div>
    </div>
</body>

<!-- Core JS Files -->
<script src="{{asset('js/jquery-3.2.1.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/popper.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Paper Kit Initialization snd functons -->
<script src="{{asset('js/paper-kit.js?v=2.1.0')}}"></script>

</html>
