@extends('admin.masters.nav-admin')
  <!-- Main content -->
  @section('content')

    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->

        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">

      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card card-stats mb-12 mb-xl-12">

            <div class="card-body">
              <div class="row">

                <div class="col">
                    <h3 class="card-title text-uppercase text-muted mb-0">BERITA</h5><br>
                      <form method="POST" action="{{url('/penulis/edit/'.$penulis->id)}}" enctype="multipart/form-data">
                                          {{ csrf_field() }}
                                          {{method_field('PUT')}}
                                      <div class="form-group">
                                          <label for="nama">NAMA</label>
                                          <input type="text" value="{{$penulis->nama}}" class="form-control" placeholder="Masukkan Nama Penulis" name="nama" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="">NIK</label>
                                          <input type="text" value="{{$penulis->nik}}" class="form-control" placeholder="Masukkan NIK" name="nik" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="nama">Email</label>
                                          <input type="text" value="{{$penulis->email}}" class="form-control" placeholder="Masukkan Email" name="email" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="nama">HP</label>
                                          <input type="text" value="{{$penulis->nohp}}" class="form-control" placeholder="Masukkan Nomor HP" name="hp" required>
                                      </div>


                                      <button type="submit" class="btn btn-primary">Edit</button>
                                  </form>
                </div>

              </div>

            </div>
          </div>
        </div>

      </div>
      <br><br>


    @endsection
