@extends('admin.masters.nav-admin')
  <!-- Main content -->
  @section('content')
    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">

      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card card-stats mb-12 mb-xl-12">

            <div class="card-body">
              <div class="row">

                <div class="col">
                    <h3 class="card-title text-uppercase text-muted mb-0">Daftar Penulis</h5><br>
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush table-responsive">
                      <thead class="thead-light">
                        <tr>
                          <th style="width:20%">NAMA</th>
                          <th style="width:20%;word-wrap: break-word;">NIK</th>
                          <th style="width:20%">Nomor HandPhone</th>
                          <th style="width:20%">Email</th>
                          <th style="width:10%">Total Artikel</th>
                          <th style="width:10%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($penulis as $penulis)
                        <tr>
                          <th scope="row">

                                <span class="mb-0 text-sm">{{$penulis->nama}}</span>

                          </th>
                          <td>
                            <div style=" ">
                            <span class="mb-0 text-xs">{{$penulis->nik}}</span></div>

                          </td>
                          <td>
{{$penulis->nohp}}
                          </td>
                          <td>
                          {{$penulis->email}}
                          </td>
                          <td>
                            <div class="d-flex align-items-center">
                              <span class="mr-2"><p class='text-primary'>{{$penulis->berita->count()}}</p></span>
                              <div>

                              </div>
                            </div>
                          </td>
                          <td class="text-right">
                            <div class="dropdown">
                              <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                
                                <a class="dropdown-item" href="{{url('/penulis/edit/'.$penulis->id)}}">Edit</a>
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach



                      </tbody>
                    </table>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>

      </div>
      <br><br>


    @endsection
