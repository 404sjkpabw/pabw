@extends('admin.masters.nav-admin')
  <!-- Main content -->
  @section('content')
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">ADMIN DASHBOARD</a>
        <!-- Form -->

        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="./assets/img/theme/team-4-800x800.jpg">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">Jessica Jones</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{ route('logout') }}" class="dropdown-item"onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="ni ni-user-run"></i>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-6 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">TOTAL BERITA</h5>
                      <span class="h2 font-weight-bold mb-0">{{count($berita)}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="ni ni-archive-2"></i>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">MENUNGGU KONFIRMASI</h5>
                      <span class="h2 font-weight-bold mb-0">{{count($waiting)}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="ni ni-time-alarm"></i>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <br><br>
            <div class="row">
            <div class="col-xl-6 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">DI PUBLIKASI</h5>
                      <span class="h2 font-weight-bold mb-0">{{count($acc)}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                        <i class="ni ni-send"></i>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">DITOLAK</h5>
                      <span class="h2 font-weight-bold mb-0">{{count($tolak)}}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="ni ni-fat-remove"></i>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">

      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card card-stats mb-12 mb-xl-12">

            <div class="card-body">
              <div class="row">

                <div class="col">
                    <h3 class="card-title text-uppercase text-muted mb-0">BERITA</h5><br>
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush table-responsive">
                      <thead class="thead-light">
                        <tr>
                          <th style="width:20%">JUDUL</th>

                          <th style="width:30%">GAMBAR</th>
                          <th style="width:30%">PENULIS</th>
                          <th style="width:10%">STATUS</th>
                          <th style="width:10%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($berita as $berita)
                        <tr>
                          <th scope="row">

                                <span class="mb-0 text-sm">{{$berita->judul}}</span>

                          </th>

                          <td>

                              <i class="bg-warning"></i> <img class="img-thumbnail" src="{{asset('images/upload/'.$berita->gambar)}}"/>

                          </td>
                          <td>
                          {{$berita->penulis->nama}}
                          </td>
                          <td>
                            <div class="d-flex align-items-center">
                              <span class="mr-2"><?php if($berita->status==1){echo "<p class='text-success'>Diterima</p>";}elseif ($berita->status==2) {
                                echo "<p class='text-danger'>Ditolak</p>";
                              }else {echo "<p class='text-primary'>Menunggu Konfirmasi</p>";} ?></span>
                              <div>

                              </div>
                            </div>
                          </td>
                          <td class="text-right">
                            <div class="dropdown">
                              <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="#">TERIMA</a>
                                <a class="dropdown-item" href="#">TOLAK</a>
                                <a class="dropdown-item" href="{{url('/berita/edit/'.$berita->id)}}">Edit</a>
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach



                      </tbody>
                    </table>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>

      </div>
      <br><br>


    @endsection
