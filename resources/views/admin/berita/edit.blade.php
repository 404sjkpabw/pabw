@extends('admin.masters.nav-admin')
  <!-- Main content -->
  @section('content')

    <!-- Top navbar -->
    
    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->

        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">

      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card card-stats mb-12 mb-xl-12">

            <div class="card-body">
              <div class="row">

                <div class="col">
                    <h3 class="card-title text-uppercase text-muted mb-0">BERITA</h5><br>
                      <form method="POST" action="{{url('/berita/edit/'.$berita->id)}}" enctype="multipart/form-data">
                                          {{ csrf_field() }}
                                          {{method_field('PUT')}}
                                      <div class="form-group">
                                          <label for="nama">Judul</label>
                                          <input type="text" value="{{$berita->judul}}" class="form-control" placeholder="Masukkan Judul Berita" name="judul" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="">Isi</label>
                                          <input type="text" value="{{$berita->isi}}" class="form-control" placeholder="Masukkan Isi Berita" name="isi" required>
                                      </div>
                                      <div class="form-group files">
                                             <label for="penulis">Penulis</label>
                                             <select class="form-control" name="penulis">
                                               @foreach($penulis as $penulis)
                                             <option value="{{$penulis->id}}">{{$penulis->nama}}</option>
                                               @endforeach
                                           </select>
                                           </div>
                                      <div class="form-group">
                                          <label for="">Gambar</label>
                                          <input type="file" value="{{asset('images/upload/'.$berita->gambar)}}" class="form-control"  name="filename" >
                                      </div>

                                      <button type="submit" class="btn btn-primary">Edit</button>
                                  </form>
                </div>

              </div>

            </div>
          </div>
        </div>

      </div>
      <br><br>


    @endsection
