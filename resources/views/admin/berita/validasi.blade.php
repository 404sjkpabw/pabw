@extends('admin.masters.nav-admin')
  <!-- Main content -->
  @section('content')
    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">

        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">

      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card card-stats mb-12 mb-xl-12">

            <div class="card-body">
              <div class="row">

                <div class="col">
                    <h3 class="card-title text-uppercase text-muted mb-0">Validasi Berita</h5><br>
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                      <thead class="thead-light">
                        <tr>
                          <th style="width:10%">JUDUL</th>
                          <th style="width:30%;word-wrap: break-word;">ISI</th>
                          <th style="width:20%">GAMBAR</th>
                          <th style="width:20%">PENULIS</th>
                          <th style="width:10%">STATUS</th>
                          <th style="width:10%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($berita as $berita)
                        <tr>
                          <th scope="row">

                                <span class="mb-0 text-sm">{{$berita->judul}}</span>

                          </th>
                          <td>
                            <div style=" ">
                            <span class="mb-0 text-xs">{{$berita->isi}}</span></div>

                          </td>
                          <td>

                              <i class="bg-warning"></i> <img class="img-thumbnail" src="{{asset('images/upload/'.$berita->gambar)}}"/>

                          </td>
                          <td>
                          {{$berita->penulis->nama}}
                          </td>
                          <td>
                            <div class="d-flex align-items-center">
                              <span class="mr-2"><?php if($berita->status==1){echo "<p class='text-success'>Diterima</p>";}elseif ($berita->status==2) {
                                echo "DItolak";
                              }else {echo "Menunggu Konfirmasi";} ?></span>
                              <div>

                              </div>
                            </div>
                          </td>
                          <td class="text-right">
                            <div class="dropdown">
                              <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <form action="{{url('validasi/'.$berita->id)}}" method="post">{{csrf_field()}} {{method_field('PUT')}}
                              <button type="submit" class="dropdown-item" value="1" name="action">TERIMA</button>
                              <button type="submit" class="dropdown-item" value="2" name="action">TOLAK</button></form>


                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach



                      </tbody>
                    </table>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>

      </div>
      <br><br>


    @endsection
