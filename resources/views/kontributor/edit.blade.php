@extends('kontributor.masters.nav-admin')
  <!-- Main content -->
  @section('content')
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">ADMIN DASHBOARD</a>
        <!-- Form -->

        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">

                  <i class="ni ni-circle-08 text-warning"></i>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{$nama}}</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>

              <div class="dropdown-divider"></div>
              <a href="{{ route('logout') }}" class="dropdown-item"onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="ni ni-user-run"></i>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->

        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">

      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card card-stats mb-12 mb-xl-12">

            <div class="card-body">
              <div class="row">

                <div class="col">
                    <h3 class="card-title text-uppercase text-muted mb-0">BERITA</h5><br>
                      <form method="POST" action="{{url('/penulis/dashboard/berita/edit/'.$berita->id)}}" enctype="multipart/form-data">
                                          {{ csrf_field() }}
                                          {{method_field('PUT')}}
                                      <div class="form-group">
                                          <label for="nama">Judul</label>
                                          <input type="text" value="{{$berita->judul}}" class="form-control" placeholder="Masukkan Judul Berita" name="judul" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="">Isi</label>
                                          <input type="text" value="{{$berita->isi}}" class="form-control" placeholder="Masukkan Isi Berita" name="isi" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="">Gambar</label>
                                          <input type="file" value="{{asset('images/upload/'.$berita->gambar)}}" class="form-control"  name="filename" >
                                      </div>

                                      <button type="submit" class="btn btn-primary">Edit</button>
                                  </form>
                </div>

              </div>

            </div>
          </div>
        </div>

      </div>
      <br><br>


    @endsection
