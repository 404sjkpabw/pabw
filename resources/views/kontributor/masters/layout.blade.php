<!doctype html>
<html>
<head>
  <title>CRUD</title>
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
</head>
<body>

    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">
            
            <li class="nav-item">
              <a class="nav-link" href="/berita">Berita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/penulis">Penulis</a>
            </li>
          </ul>
          <span class="navbar-text">
            Website Berita Paling Mantul!
          </span>
        </div>
      </nav>
        @yield('header')

        @yield('content')
    </div>

</body>
</html>
