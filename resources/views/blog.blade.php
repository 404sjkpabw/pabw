<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="../assets/img/favicon.ico">
	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>PORTAL EDUKASI KESIAPSIAGAAN BENCANA</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/paper-kit.css?v=2.1.0')}}">


	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="{{asset('css/demo.css')}}" rel="stylesheet" />

	<!--     Fonts and icons     -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{asset('css/nucleo-icons.css')}}" rel="stylesheet">

</head>

<body>
	<nav class="navbar navbar-expand-lg fixed-top bg-danger" >
		<div class="container">
			<div class="navbar-translate">
				<a class="navbar-brand" href="/#depan">404 - TEAM</a>
				<button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a href="/#depan" target="_self"  class="nav-link">HOME</a>
					</li>
					<li class="nav-item">
						<a href="/#siaga" target="_self" class="nav-link">SIAGA</a>
					</li>
					<li class="nav-item">
						<a href="/#berita" target="_self" class="nav-link"></i>BERITA</a>
					</li>
					<li class="nav-item">
						<a href="/#team" target="_self" class="nav-link"></i>TEAM</a>
					</li>
					<li class="nav-item">
						<a href="/masuk" target="_self" class="nav-link"></i>LOGIN</a>
					</li>

				</ul>
			</div>
		</div>
	</nav>
<section id="depan">
	<div class="page-header" data-parallax="true" style="background-image: url('{{asset('images/daniel-olahh.jpg')}}');">
		<div class="filter"></div>
		<div class="container">
			<div class="motto text-center">
				<h1>PORTAL EDUKASI KESIAPSIAGAAN BENCANA</h1>

				<br />

			</div>
		</div>
	</div>
</section>
	<div class="main">
		<div class="section" id="siaga">
			<div class="container-fluid">

				<div class="row">
            <div class="col-md-10 ml-auto mr-auto">
              <div class="text-center">

                <a href="javascrip: void(0);">
                  <h3 class="title">{{$berita->judul}}</h3>
                </a>

              </div>
            </div>
            <div class="col-md-8 ml-auto mr-auto">
              <a href="javascrip: void(0);">
                <div class="card" data-radius="none" >
									<img src="{{asset('images/upload/'.$berita->gambar)}}" class="img-thumbnail" style="max-height:500px;"></img>
								</div>

              </a>
              <div class="article-content">
                {{$berita->isi}}

              </div>
              <br>

              <hr>

            </div>
          </div>

			</div>
		</div>



	</div>
	<footer class="footer section-dark">
		<div class="container">
			<div class="row">

				<div class="credits ml-auto">
					<span class="copyright">
						© <script>
							document.write(new Date().getFullYear())
						</script>, made with <i class="fa fa-heart heart"></i> by 404 Team
					</span>
				</div>
			</div>
		</div>
	</footer>
</body>

<!-- Core JS Files -->

<script src="{{asset('js/jquery-3.2.1.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/popper.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Paper Kit Initialization snd functons -->
<script src="{{asset('js/paper-kit.js?v=2.1.0')}}"></script>

</html>
